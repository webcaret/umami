#!/bin/bash

# Install Drupal
cd /var/www/html

# install dependencies
composer install

# Build env.
./vendor/bin/phing build-dev

# Installs Drupal
./vendor/bin/phing install-dev

# Make sure nginx can change the files
chown -R www-data:www-data web/sites/default/files

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
